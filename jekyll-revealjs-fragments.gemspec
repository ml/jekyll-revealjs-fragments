# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "jekyll/revealjs/fragments/version"

Gem::Specification.new do |spec|
  spec.name          = "jekyll-revealjs-fragments"
  spec.version       = Jekyll::Revealjs::Fragments::VERSION
  spec.authors       = ["Marc Löchner"]
  spec.email         = ["marc.loechner@tu-dresden.de"]

  spec.summary       = "Creates fragments for reveal.js presentations created with Jekyll"
  spec.homepage      = "https://gitlab.vgiscience.de/ml/jekyll-revealjs-fragments"

end
