# Jekyll::Revealjs::Fragments

Fragments allow slide elements to come one by one. This is often used in lists to subsequently showfragments of a list during a presentation.

To use fragments, jekyll-reveal.js includes a jekyll-plugin, that simplifies the use of fragments in markdown. To specify the current element as a fragment, use the {% fragment %}-tag like this:

    # Slide

    * This {% fragment %}
    * will {% fragment %}
    * come one by one {% fragment %}

The the code originates from [Dennis Ploegers jekyll-revealjs](https://github.com/dploeger/jekyll-revealjs)
