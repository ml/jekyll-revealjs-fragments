require "jekyll/revealjs/fragments/version"

module Jekyll
  module Revealjs
    module Fragments
        class RenderFragment < Liquid::Tag

            def initialize(tag_name, text, tokens)
                super
                @text = text
            end

            def render(context)
              "<!-- .element: class=\"fragment #{@text}\" -->"
            end

        end
    end
  end
end

Liquid::Template.register_tag('fragment', Jekyll::Revealjs::Fragments::RenderFragment)
